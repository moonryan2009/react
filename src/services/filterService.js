import useAppContext from '../hooks/useAppContext';
import dayjs from 'dayjs';

const useFilterService = () => {
  const { store, updateStore } = useAppContext();

  const storeFilter = (startDateTime, endDateTime, clients, filter, optionValue) => {
    const newFilterGroup = {
      clients,
      filter: { [filter.value]: optionValue }
    };

    const updatedFiltersList = store.filtersList ? JSON.parse(store.filtersList) : [];

    const existingFilterIndex = updatedFiltersList.findIndex(
      (f) => f.startDateTime === startDateTime && f.endDateTime === endDateTime
    );

    if (existingFilterIndex !== -1) {
      updatedFiltersList[existingFilterIndex].filterGroup.push(newFilterGroup);
    } else {
      updatedFiltersList.push({
        startDateTime,
        endDateTime,
        filterGroup: [newFilterGroup],
      });
    }

    updateStore('filtersList', JSON.stringify(updatedFiltersList));
  };

  const deleteFilter = (index) => {
    const updatedFiltersList = (store.filtersList ? JSON.parse(store.filtersList) : []).filter((_, i) => i !== index);
    updateStore('filtersList', JSON.stringify(updatedFiltersList));
  };

  const getFilters = () => {
    return store.filtersList ? JSON.parse(store.filtersList) : [];
  };

  const getTimes = () => {
    return {
      start: store.startDateTime ? dayjs(store.startDateTime) : null,
      end: store.endDateTime ? dayjs(store.endDateTime) : null,
    };
  };

  const getFilterArrays = () => {
    const filtersList = getFilters();
    return filtersList.map(filter => ({
      start: filter.startDateTime,
      end: filter.endDateTime,
      filterGroup: filter.filterGroup.map(group => ({
        clients: group.clients.map(client => client.label).join(', '),
        filter: Object.entries(group.filter).map(([key, value]) => ({ key, value }))
      }))
    }));
  };

  return {
    storeFilter,
    deleteFilter,
    getFilters,
    getTimes,
    getFilterArrays,
  };
};

export default useFilterService;
