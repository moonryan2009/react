import React, { useState } from 'react';
import { Box, Button, Grid } from '@mui/material';
import CustomSearchableSelectBox from './common/CustomSearchableSelectBox';
import CustomTextField from './common/CustomTextField';
import CustomDateTimePicker from './common/CustomDateTimePicker';
import useFilterService from '../services/filterService';
import dayjs from 'dayjs';

const clients = [
  { label: 'Client 1', value: 'client1' },
  { label: 'Client 2', value: 'client2' },
  { label: 'Client 3', value: 'client3' },
  // Add more clients as needed
];

const filters = [
  { label: 'Filter 1', value: 'filter1' },
  { label: 'Filter 2', value: 'filter2' },
  { label: 'Filter 3', value: 'filter3' },
  // Add more filters as needed
];

const FilterInputForm = () => {
  const { storeFilter } = useFilterService();
  const [selectedClient, setSelectedClient] = useState(null);
  const [selectedFilter, setSelectedFilter] = useState(null);
  const [optionValue, setOptionValue] = useState('');
  const [startDateTime, setStartDateTime] = useState(dayjs().subtract(7, 'day'));
  const [endDateTime, setEndDateTime] = useState(dayjs());

  const handleApply = () => {
    storeFilter(startDateTime, endDateTime, selectedClient ? [selectedClient] : [], selectedFilter, optionValue);
  };

  return (
    <Box>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <CustomDateTimePicker
            label="Start Date and Time"
            value={startDateTime}
            onChange={(newValue) => setStartDateTime(newValue)}
            helperText="Please select a start date and time"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <CustomDateTimePicker
            label="End Date and Time"
            value={endDateTime}
            onChange={(newValue) => setEndDateTime(newValue)}
            helperText="Please select an end date and time"
          />
        </Grid>
        <Grid item xs={12}>
          <CustomSearchableSelectBox
            options={clients}
            label="Select a Client"
            value={selectedClient}
            onChange={(newValue) => setSelectedClient(newValue)}
            helperText="Please select a client"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <CustomSearchableSelectBox
            options={filters}
            label="Select a Filter"
            value={selectedFilter}
            onChange={(newValue) => setSelectedFilter(newValue)}
            helperText="Please select a filter (optional)"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <CustomTextField
            label="Option"
            value={optionValue}
            onChange={(event) => setOptionValue(event.target.value)}
            helperText="Please enter an option (optional)"
          />
        </Grid>
        <Grid item xs={12}>
          <Button variant="contained" onClick={handleApply}>Apply</Button>
        </Grid>
      </Grid>
    </Box>
  );
};

export default FilterInputForm;
