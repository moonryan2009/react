// src/components/SubPanel.js
import React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Box from '@mui/material/Box';

const SubPanel = ({ menuItems }) => {
  return (
    <AppBar position="static" color="default">
      <Toolbar>
        <Box>
          {menuItems.map((item, index) => (
            <Tooltip key={index} title={item.label} arrow>
              <IconButton color="inherit" onClick={item.onClick}>
                {item.icon}
              </IconButton>
            </Tooltip>
          ))}
        </Box>
      </Toolbar>
    </AppBar>
  );
};

export default SubPanel;
