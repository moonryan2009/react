// src/components/Layout.js
import React from 'react';
import { Outlet } from 'react-router-dom';
import Header from './Header';
import SubPanel from './SubPanel';
import { useSubPage } from '../contexts/SubPageContext';

const Layout = () => {
  const { subPageTitle, menuItems } = useSubPage();

  return (
    <div>
      <Header subPageTitle={subPageTitle} />
      <SubPanel menuItems={menuItems} />
      <main>
        <Outlet />
      </main>
    </div>
  );
};

export default Layout;
