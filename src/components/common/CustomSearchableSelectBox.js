import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';

const CustomSearchableSelectBox = ({
  options = [],
  label = '',
  value = null,
  onChange = () => {},
  helperText = ''
}) => {
  return (
    <Autocomplete
      options={options}
      getOptionLabel={(option) => option.label}
      value={value}
      onChange={(event, newValue) => onChange(newValue)}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          helperText={helperText}
          variant="standard"
        />
      )}
    />
  );
};

CustomSearchableSelectBox.propTypes = {
  options: PropTypes.arrayOf(PropTypes.object),
  label: PropTypes.string,
  value: PropTypes.any,
  onChange: PropTypes.func,
  helperText: PropTypes.string,
};

export default CustomSearchableSelectBox;
