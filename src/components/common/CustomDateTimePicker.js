import React from 'react';
import PropTypes from 'prop-types';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { TextField } from '@mui/material';

const CustomDateTimePicker = ({
  label = '',
  value = null,
  onChange = () => {},
  helperText = ''
}) => {
  return (
    <DateTimePicker
      label={label}
      value={value}
      onChange={onChange}
      slots={{
        textField: TextField,
      }}
      slotProps={{
        textField: {
          helperText: helperText,
          variant: 'standard',
        },
      }}
    />
  );
};

CustomDateTimePicker.propTypes = {
  label: PropTypes.string,
  value: PropTypes.any,
  onChange: PropTypes.func,
  helperText: PropTypes.string,
};

export default CustomDateTimePicker;
