// src/components/common/CustomToolbar.js
import * as React from 'react';
import IconButton from '@mui/material/IconButton';
import TextField from '@mui/material/TextField';
import { GridToolbarContainer, GridToolbarExport } from '@mui/x-data-grid';
import ClearIcon from '@mui/icons-material/Clear';
import SearchIcon from '@mui/icons-material/Search';
import { styled } from '@mui/system';
import DownloadIcon from '@mui/icons-material/Download';
import Typography from '@mui/material/Typography';

const ToolbarContainer = styled(GridToolbarContainer)(({ theme }) => ({
  padding: theme.spacing(0.5, 0.5, 0),
  justifyContent: 'space-between',
  display: 'flex',
  alignItems: 'flex-start',
  flexWrap: 'wrap',
}));

const SearchTextField = styled(TextField)(({ theme }) => ({
  [theme.breakpoints.down('xs')]: {
    width: '100%',
  },
  margin: theme.spacing(1, 0.5, 1.5),
  '& .MuiSvgIcon-root': {
    marginRight: theme.spacing(0.5),
  },
  '& .MuiInput-underline:before': {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
}));

const CustomToolbar = (props) => {
  const { searchText, onSearch, clearSearch } = props;

  return (
    <ToolbarContainer>
      <SearchTextField
        variant="standard"
        value={searchText}
        onChange={onSearch}
        placeholder="Search…"
        InputProps={{
          startAdornment: <SearchIcon fontSize="small" />,
          endAdornment: (
            <IconButton
              title="Clear"
              aria-label="Clear"
              size="small"
              style={{ visibility: searchText ? 'visible' : 'hidden' }}
              onClick={clearSearch}
            >
              <ClearIcon fontSize="small" />
            </IconButton>
          ),
        }}
      />
      <GridToolbarExport
        csvOptions={{
          fields: ['id', 'name', 'age', 'email'],
        }}
        components={{
          ExportButton: ({ onClick }) => (
            <IconButton onClick={onClick} title="Export">
              <DownloadIcon />
              <Typography variant="body2" style={{ marginLeft: 4 }}>
                Export
              </Typography>
            </IconButton>
          ),
        }}
      />
    </ToolbarContainer>
  );
};

export default CustomToolbar;
