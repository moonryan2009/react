// src/components/CustomTabs.js
import React from 'react';
import PropTypes from 'prop-types';
import { Tabs, Tab, Box } from '@mui/material';

const CustomTabs = ({ tabs, value, onChange }) => (
  <Box>
    <Tabs value={value} onChange={onChange} aria-label="custom tabs">
      {tabs.map((tab, index) => (
        <Tab key={index} label={tab.label} />
      ))}
    </Tabs>
    {tabs.map((tab, index) => (
      <div key={index} hidden={value !== index}>
        {value === index && (
          <Box p={3}>
            <tab.component />
          </Box>
        )}
      </div>
    ))}
  </Box>
);

CustomTabs.propTypes = {
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      component: PropTypes.elementType.isRequired,
    })
  ).isRequired,
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default CustomTabs;
