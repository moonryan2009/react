// src/components/CustomTextField.js
import React from 'react';
import PropTypes from 'prop-types';
import { TextField, styled } from '@mui/material';

const StyledTextField = styled(TextField)(({ theme }) => ({
  '& .MuiOutlinedInput-root': {
    '& fieldset': {
      borderColor: theme.palette.divider,
    },
    '&:hover fieldset': {
      borderColor: theme.palette.text.primary,
    },
    '&.Mui-focused fieldset': {
      borderColor: theme.palette.primary.main,
    },
  },
}));

const CustomTextField = ({ label, value, onChange, ...rest }) => (
  <StyledTextField
    label={label}
    value={value}
    onChange={onChange}
    variant="outlined"
    fullWidth
    {...rest}
  />
);

CustomTextField.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default CustomTextField;
