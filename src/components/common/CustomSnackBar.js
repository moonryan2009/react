// src/components/CustomSnackbar.js
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Snackbar, SnackbarContent, Button } from '@mui/material';

const CustomSnackbar = ({ message, action, duration, open, onClose }) => (
  <Snackbar
    open={open}
    autoHideDuration={duration}
    onClose={onClose}
    action={action}
  >
    <SnackbarContent
      message={message}
      action={
        <Button color="inherit" size="small" onClick={onClose}>
          Close
        </Button>
      }
    />
  </Snackbar>
);

CustomSnackbar.propTypes = {
  message: PropTypes.string.isRequired,
  action: PropTypes.node,
  duration: PropTypes.number,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

CustomSnackbar.defaultProps = {
  action: null,
  duration: 6000,
};

export default CustomSnackbar;
