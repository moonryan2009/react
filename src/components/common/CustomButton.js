// src/components/common/CustomButton.js
import React from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';

const CustomButton = ({ onClick, children, type = 'button', variant = 'contained', color = 'primary', className = '', disabled = false }) => {
  return (
    <Button
      type={type}
      onClick={onClick}
      variant={variant}
      color={color}
      className={className}
      disabled={disabled}
    >
      {children}
    </Button>
  );
};

CustomButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
  type: PropTypes.string,
  variant: PropTypes.oneOf(['text', 'outlined', 'contained']),
  color: PropTypes.oneOf(['default', 'inherit', 'primary', 'secondary']),
  className: PropTypes.string,
  disabled: PropTypes.bool,
};

export default CustomButton;
