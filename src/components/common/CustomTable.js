// src/components/common/CustomTable.js
import * as React from 'react';
import PropTypes from 'prop-types';
import {
  Table, TableBody, TableCell, TableContainer, TableHead, TableRow,
  TableSortLabel, TablePagination, Toolbar, TextField, IconButton, Paper, Tooltip
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import ClearIcon from '@mui/icons-material/Clear';
import DownloadIcon from '@mui/icons-material/Download';
import { CSVLink } from 'react-csv';
import { styled } from '@mui/system';

const SearchTextField = styled(TextField)(({ theme }) => ({
  [theme.breakpoints.down('xs')]: {
    width: '100%',
  },
  margin: theme.spacing(1, 0.5, 1.5),
  '& .MuiSvgIcon-root': {
    marginRight: theme.spacing(0.5),
  },
  '& .MuiInput-underline:before': {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
}));

const ExportIcon = styled(DownloadIcon)(({ theme }) => ({
  color: theme.palette.text.primary,
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const CustomTable = ({
  columns,
  rows,
  enableSearch = true,
  enableDownload = true,
  enableSorting = true,
  enablePagination = true,
  enableAggregate = true
}) => {
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState(columns[0].field);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [searchText, setSearchText] = React.useState('');

  const handleRequestSort = (property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSearchChange = (event) => {
    setSearchText(event.target.value);
  };

  const handleClearSearch = () => {
    setSearchText('');
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const filteredRows = rows.filter((row) => {
    return Object.keys(row).some((key) =>
      String(row[key]).toLowerCase().includes(searchText.toLowerCase())
    );
  });

  const sortedRows = enableSorting
    ? stableSort(filteredRows, getComparator(order, orderBy))
    : filteredRows;

  const aggregatedData = columns.map(column => {
    if (column.aggregate) {
      return {
        field: column.field,
        value: sortedRows.reduce((acc, row) => acc + (Number(row[column.field]) || 0), 0)
      };
    }
    return null;
  }).filter(Boolean);

  const CSVLinkWithRef = React.forwardRef((props, ref) => (
    <CSVLink {...props} innerRef={ref} />
  ));

  return (
    <Paper style={{ width: '100%', padding: '16px' }}>
      <Toolbar>
        {enableSearch && (
          <SearchTextField
            variant="standard"
            value={searchText}
            onChange={handleSearchChange}
            placeholder="Search…"
            InputProps={{
              startAdornment: <SearchIcon fontSize="small" />,
              endAdornment: (
                <IconButton
                  title="Clear"
                  aria-label="Clear"
                  size="small"
                  style={{ visibility: searchText ? 'visible' : 'hidden' }}
                  onClick={handleClearSearch}
                >
                  <ClearIcon fontSize="small" />
                </IconButton>
              ),
            }}
          />
        )}
        {enableDownload && (
          <div style={{ marginLeft: 'auto', display: 'flex', alignItems: 'center' }}>
            <Tooltip title="Export">
              <CSVLinkWithRef
                data={sortedRows}
                headers={columns.map((column) => ({ label: column.headerName, key: column.field }))}
                filename="data.csv"
                style={{ textDecoration: 'none', display: 'flex', alignItems: 'center' }}
              >
                <ExportIcon />
              </CSVLinkWithRef>
            </Tooltip>
          </div>
        )}
      </Toolbar>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.field}
                  sortDirection={orderBy === column.field ? order : false}
                >
                  {enableSorting ? (
                    <TableSortLabel
                      active={orderBy === column.field}
                      direction={orderBy === column.field ? order : 'asc'}
                      onClick={() => handleRequestSort(column.field)}
                    >
                      {column.headerName}
                    </TableSortLabel>
                  ) : (
                    column.headerName
                  )}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {sortedRows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => (
                <TableRow key={index}>
                  {columns.map((column) => (
                    <TableCell key={column.field}>{row[column.field]}</TableCell>
                  ))}
                </TableRow>
              ))}
            {enableAggregate && (
              <TableRow>
                {columns.map((column) => {
                  const aggregate = aggregatedData.find(a => a.field === column.field);
                  return (
                    <TableCell key={column.field}>
                      {aggregate ? aggregate.value : ''}
                    </TableCell>
                  );
                })}
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      {enablePagination && (
        <TablePagination
          rowsPerPageOptions={[5, 10, 20]}
          component="div"
          count={sortedRows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      )}
    </Paper>
  );
};

CustomTable.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  rows: PropTypes.arrayOf(PropTypes.object).isRequired,
  enableSearch: PropTypes.bool,
  enableDownload: PropTypes.bool,
  enableSorting: PropTypes.bool,
  enablePagination: PropTypes.bool,
  enableAggregate: PropTypes.bool,
};

export default CustomTable;
