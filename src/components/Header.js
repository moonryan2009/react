// src/components/Header.js
import React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import SettingsIcon from '@mui/icons-material/Settings';
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import { useTheme } from '@mui/material/styles';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { useThemeToggle } from '../contexts/ThemeContext';
import { useSidePanel } from '../contexts/SidePanelContext';
import SidePanel from './SidePanel';


const Header = ({ subPageTitle }) => {
  const theme = useTheme();
  const toggleTheme = useThemeToggle();
  const { sidePanelOpen, openSidePanel, closeSidePanel } = useSidePanel();

  const menuItems = [
    { icon: <SettingsIcon />, label: 'Settings', onClick: () => console.log('Item 1 clicked') },
    { icon: <MenuIcon />, label: 'Menu Item 2', onClick: () => console.log('Item 2 clicked') },
  ];

  return (
    <div style={{ flexGrow: 1 }}>
      <AppBar position="fixed">
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu" onClick={openSidePanel}>
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" style={{ flexGrow: 1 }}>
            My App {subPageTitle && `- ${subPageTitle}`}
          </Typography>
          <IconButton color="inherit" onClick={toggleTheme}>
            {theme.palette.mode === 'dark' ? <Brightness7Icon /> : <Brightness4Icon />}
          </IconButton>
        </Toolbar>
      </AppBar>
      <div style={{ marginTop: 64 }}></div>
      <SidePanel open={sidePanelOpen} onClose={closeSidePanel} position="left" width={250}>
        {/* Add your generic menu content here */}
        {menuItems.map((item, index) => (
          <ListItem button key={index} onClick={item.onClick}>
          <ListItemIcon>{item.icon}</ListItemIcon>
          <ListItemText primary={item.label} />
          </ListItem>
        ))}
      </SidePanel>
    </div>
  );
};

export default Header;
