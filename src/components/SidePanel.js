// src/components/SidePanel.js
import React from 'react';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Box from '@mui/material/Box';

const SidePanel = ({ open, onClose, position = 'left', width = 250, children }) => {
  return (
    <Drawer anchor={position} open={open} onClose={onClose} PaperProps={{ style: { width } }}>
      <Box display="flex" justifyContent="flex-end" p={1}>
        <IconButton onClick={onClose}>
          <CloseIcon />
        </IconButton>
      </Box>
      <Box p={2}>
        {children}
      </Box>
    </Drawer>
  );
};

export default SidePanel;
