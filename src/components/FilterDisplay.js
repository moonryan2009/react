import React from 'react';
import { Box, Typography, IconButton } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import useFilterService from '../services/filterService';
import CustomTable from './common/CustomTable';

const FilterDisplay = () => {
  const { getFilterArrays, deleteFilter, getTimes } = useFilterService();
  const filtersList = getFilterArrays();
  const { start, end } = getTimes();

  const columns = [
    { field: 'client', headerName: 'Client' },
    { field: 'filter', headerName: 'Filter' },
    { field: 'value', headerName: 'Value' },
    { field: 'actions', headerName: 'Actions' },
  ];

  const rows = filtersList.flatMap((filterEntry, filterIndex) =>
    filterEntry.filterGroup.flatMap((group, groupIndex) =>
      group.filter.map((entry, index) => ({
        id: `${filterIndex}-${groupIndex}-${index}`,
        client: group.clients,
        filter: entry.key,
        value: entry.value,
        actions: (
          <IconButton onClick={() => deleteFilter(filterIndex)}>
            <DeleteIcon />
          </IconButton>
        ),
      }))
    )
  );

  if (filtersList.length === 0) {
    return null;
  }

  return (
    <Box>
      <Typography variant="h6">Stored Filters:</Typography>
      <Typography variant="subtitle1">Start Date and Time: {start ? start.toString() : 'None'}</Typography>
      <Typography variant="subtitle1">End Date and Time: {end ? end.toString() : 'None'}</Typography>
      <CustomTable
        columns={columns}
        rows={rows}
        enableSearch={false}
        enableDownload={false}
        enableSorting={false}
        enablePagination={false}
        enableAggregate={false}
      />
    </Box>
  );
};

export default FilterDisplay;
