// src/contexts/SubPageContext.js
import React, { createContext, useState, useContext } from 'react';

const SubPageContext = createContext();

export const useSubPage = () => useContext(SubPageContext);

export const SubPageProvider = ({ children }) => {
  const [subPageTitle, setSubPageTitle] = useState('');
  const [menuItems, setMenuItems] = useState([]);

  return (
    <SubPageContext.Provider value={{ subPageTitle, setSubPageTitle, menuItems, setMenuItems }}>
      {children}
    </SubPageContext.Provider>
  );
};
