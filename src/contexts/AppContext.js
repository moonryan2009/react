import React, { createContext, useContext, useState, useEffect } from 'react';
import dayjs from 'dayjs';

const AppContext = createContext();

export const useAppContext = () => useContext(AppContext);

export const AppProvider = ({ children }) => {
  const [store, setStore] = useState({});

  useEffect(() => {
    const storedData = localStorage.getItem('appStore');
    if (storedData) {
      setStore(JSON.parse(storedData));
    }
  }, []);

  const updateStore = (key, value) => {
    const newStore = { ...store, [key]: value };
    setStore(newStore);
    localStorage.setItem('appStore', JSON.stringify(newStore));
  };

  return (
    <AppContext.Provider value={{ store, updateStore }}>
      {children}
    </AppContext.Provider>
  );
};

export { AppContext };
