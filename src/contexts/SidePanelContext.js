// src/contexts/SidePanelContext.js
import React, { createContext, useState, useContext } from 'react';

const SidePanelContext = createContext();

export const useSidePanel = () => useContext(SidePanelContext);

export const SidePanelProvider = ({ children }) => {
  const [sidePanelOpen, setSidePanelOpen] = useState(false);

  const openSidePanel = () => setSidePanelOpen(true);
  const closeSidePanel = () => setSidePanelOpen(false);

  return (
    <SidePanelContext.Provider value={{ sidePanelOpen, openSidePanel, closeSidePanel }}>
      {children}
    </SidePanelContext.Provider>
  );
};
