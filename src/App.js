// src/App.js
import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { ThemeToggleProvider } from './contexts/ThemeContext';
import { SidePanelProvider } from './contexts/SidePanelContext';
import { SubPageProvider } from './contexts/SubPageContext';
import { AppProvider } from './contexts/AppContext';
import Layout from './components/Layout';
import TestPage from './pages/TestPage';
import ExamplePage from './pages/ExamplePage';
import FilterPage from './pages/FilterPage';
import { Filter } from '@mui/icons-material';

function App() {
  return (
    <AppProvider>
    <LocalizationProvider dateAdapter={AdapterDayjs}>
    <ThemeToggleProvider>
      <SidePanelProvider>
        <SubPageProvider>
          <Router>
            <Routes>
              <Route path="/" element={<Layout />}>
                <Route index element={<div><h1>Welcome to My App</h1></div>} />
                <Route path="test" element={<TestPage />} />
                <Route path="example" element={<ExamplePage />} />
                <Route path="filter" element={<FilterPage />} />
              </Route>
            </Routes>
          </Router>
        </SubPageProvider>
      </SidePanelProvider>
    </ThemeToggleProvider>
    </LocalizationProvider>
    </AppProvider>
  );
}

export default App;
