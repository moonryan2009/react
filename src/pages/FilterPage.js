// src/pages/FilterPage.js
import React from 'react';
import { Box } from '@mui/material';
import FilterInputForm from '../components/FilterInputForm';
import FilterDisplay from '../components/FilterDisplay';

const FilterPage = () => {
  return (
    <Box sx={{ padding: 2 }}>
      <h1>Filter Page</h1>
      <FilterInputForm />
      <FilterDisplay />
    </Box>
  );
};

export default FilterPage;
