// src/pages/ExamplePage.js
import React, { useState } from 'react';
import CustomDateTimePicker from '../components/common/CustomDateTimePicker';
import CustomSearchableSelectBox from '../components/common/CustomSearchableSelectBox';
import CustomTextField from '../components/common/CustomTextField';
import dayjs from 'dayjs';

const options = [
  { label: 'Option 1', value: 'option1' },
  { label: 'Option 2', value: 'option2' },
  { label: 'Option 3', value: 'option3' },
  // Add more options as needed
];

const ExamplePage = () => {
  const [selectedDateTime, setSelectedDateTime] = useState(dayjs());
  const [selectedOption, setSelectedOption] = useState(null);
  const [textFieldValue, setTextFieldValue] = useState('');

  const handleDateTimeChange = (newDateTime) => {
    setSelectedDateTime(newDateTime);
  };

  const handleSelectChange = (newValue) => {
    setSelectedOption(newValue);
  };

  const handleTextFieldChange = (event) => {
    setTextFieldValue(event.target.value);
  };

  return (
    <div>
      <h1>Example Page with DateTime Picker, Select Box, and Text Field</h1>
      <CustomDateTimePicker
        label="Select Date and Time"
        value={selectedDateTime}
        onChange={handleDateTimeChange}
        helperText="Please select a date and time"
      />
      <CustomSearchableSelectBox
        options={options}
        label="Select an Option"
        value={selectedOption}
        onChange={handleSelectChange}
        helperText="Please select an option from the list"
      />
      <CustomTextField
        label="Enter Text"
        value={textFieldValue}
        onChange={handleTextFieldChange}
        helperText="Please enter some text"
      />
      <div>
        <h2>Selected Values:</h2>
        <p>Selected Date and Time: {selectedDateTime ? selectedDateTime.toString() : 'None'}</p>
        <p>Selected Option: {selectedOption ? selectedOption.label : 'None'}</p>
        <p>Text Field Value: {textFieldValue}</p>
      </div>
    </div>
  );
};

export default ExamplePage;
