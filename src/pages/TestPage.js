// src/pages/TestPage.js
import React, { useEffect, useState } from 'react';
import Container from '@mui/material/Container';
import SaveIcon from '@mui/icons-material/Save';
import OpenInBrowserIcon from '@mui/icons-material/OpenInBrowser';
import CloseIcon from '@mui/icons-material/Close';
import { useSubPage } from '../contexts/SubPageContext';
import Box from '@mui/material/Box';
import CustomButton from '../components/common/CustomButton';
import CustomTable from '../components/common/CustomTable';
import CustomSearchableSelectBox from '../components/common/CustomSearchableSelectBox';

const TestPage = () => {
  const { setSubPageTitle, setMenuItems } = useSubPage();

  useEffect(() => {
    setSubPageTitle('Test Page');
    setMenuItems([
      { icon: <OpenInBrowserIcon />, label: 'Open', onClick: () => console.log('Open clicked') },
      { icon: <SaveIcon />, label: 'Save', onClick: () => console.log('Save clicked') },
      { icon: <CloseIcon />, label: 'Close', onClick: () => console.log('Close clicked') },
    ]);
  }, [setSubPageTitle, setMenuItems]);

  const handleClick = () => {
    alert('Button clicked!');
  };

  const [selectedValue, setSelectedValue] = useState(null);

  const handleSelectChange = (newValue) => {
    setSelectedValue(newValue);
  };

  const columns = [
    { field: 'id', headerName: 'ID', width: 90 },
    { field: 'name', headerName: 'Name', width: 150 },
    { field: 'age', headerName: 'Age', width: 110 },
    { field: 'email', headerName: 'Email', width: 200 },
  ];
  
  const rows = [
    { id: 1, name: 'Alice', age: 25, email: 'alice@example.com' },
    { id: 2, name: 'Bob', age: 30, email: 'bob@example.com' },
    { id: 3, name: 'Charlie', age: 35, email: 'charlie@example.com' },
    // Add more rows as needed
  ];
  const options = [
    { label: 'Option 1', value: 'option1' },
    { label: 'Option 2', value: 'option2' },
    { label: 'Option 3', value: 'option3' },
    // Add more options as needed
  ];


  return (
    <div>
      <h1>Example Page</h1>
      <CustomSearchableSelectBox
        options={options}
        label="Select an Option"
        value={selectedValue}
        onChange={handleSelectChange}
        helperText="Please select an option from the list"
      />
      <div>Selected Value: {selectedValue ? selectedValue.label : 'None'}</div>
    </div>
  
  );
};

export default TestPage;
